package cn.lisor.graduationproject.utils;

import android.graphics.Bitmap;
import android.graphics.Matrix;

import java.util.regex.Pattern;

public abstract class Utils {

    public static Bitmap GetSmallerBitmap(Bitmap bitmap) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        float s = 1;
        while (w * h > 160000) {
            w >>= 1;
            h >>= 1;
            s *= 2;
        }
        Matrix matrix = new Matrix();
        matrix.postScale(1 / s, 1 / s);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    private static final Pattern urlPattern = Pattern.compile("^https?://.+$");

    public static boolean isURL(String s) {
        return urlPattern.matcher(s).matches();
    }


    public static boolean IsOneOf(int i, int... ints) {
        for (int ii : ints)
            if (i == ii) return true;
        return false;
    }
}
