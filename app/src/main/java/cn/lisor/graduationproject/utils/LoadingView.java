package cn.lisor.graduationproject.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ViewFlipper;

public abstract class LoadingView {
    // todo : error view

    // Button -> ProgressBar -> ResultView
    private ViewFlipper vf;

    private Button button;
    private ProgressBar progressBar;
    private TextView textView;

    // AsyncTask
    private _AsyncTask asyncTask;


    public LoadingView(Context context) {
        this(context, null);
    }

    public LoadingView(Context context, String button_name) {
        // Create AsyncTask
        this.asyncTask = new _AsyncTask();

        // Create ViewSwitcher
        vf = new ViewFlipper(context);

        // Create And Add Button
        if (button_name != null) {
            this.button = new Button(context);
            this.button.setText(button_name);
            this.button.setOnClickListener(v -> {
                vf.showNext();
                asyncTask.execute();
            });
            vf.addView(this.button);
        }

        // Create And Add ProgressBar
        this.progressBar = new ProgressBar(context);
        vf.addView(this.progressBar);

        // Create And Add TextView
        this.textView = new TextView(context);
        this.textView.setTextIsSelectable(true);
        vf.addView(this.textView);
    }

    public final View GetView() {
        return vf;
    }

    // Only Work When No Button
    public final LoadingView Start() {
        if (this.button == null) {
            asyncTask.execute();
        }
        return this;
    }

    public final void ShowAsDialog(String title) {
        this.SetPadding(30,0,30,0);
        AlertDialog dialog = new AlertDialog.Builder(vf.getContext())
                .setTitle(title)
                .setView(vf)
                .setNegativeButton("关闭", null)
                .create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        this.Start();
    }

    public final LoadingView SetPadding(int left, int top, int right, int bottom) {
        Context context = this.vf.getContext();
        this.vf.setPadding(
                Utils4A.dp(context, left),
                Utils4A.dp(context, top),
                Utils4A.dp(context, right),
                Utils4A.dp(context, bottom));
        return this;
    }

    protected void preDo() {
    }

    protected abstract String Do();

    protected void postDo(String result) {
    }

    private class _AsyncTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            preDo();
        }


        @Override
        protected String doInBackground(Void... voids) {
            return Do();
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            postDo(result);
            textView.setText(result);
            vf.showNext();
        }
    }

}
