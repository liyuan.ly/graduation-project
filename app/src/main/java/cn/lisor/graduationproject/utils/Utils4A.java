package cn.lisor.graduationproject.utils;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.TextView;

// Utils For Android
public class Utils4A {
    public static String GetTextByViewId(Activity a, int id) {
        return ((TextView) a.findViewById(id)).getText().toString();
    }

    public static int dp(Context context, int dp) {
        return (int) (30 * context.getResources().getDisplayMetrics().density + 0.5f);
    }

    // For Log
    public static void ExceptionLog(String str, Throwable throwable) {
        Log.w("Exception", str, throwable);
    }
}
