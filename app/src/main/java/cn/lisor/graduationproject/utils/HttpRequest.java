package cn.lisor.graduationproject.utils;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class HttpRequest {
    public enum Protocol {
        HTTP, HTTPS;
    }

    public enum Method {
        GET, POST;
    }

    private Protocol protocol;
    private Method method;
    private String host;
    private String path;

    private String body;
    private List<Pair<String, String>> header = new ArrayList<>();
    private List<Pair<String, String>> query = new ArrayList<>();

    private static final String InfoLogTag = "HttpRequest";


    public HttpRequest(Protocol protocol, Method method, String host, String path) {
        this.protocol = protocol;
        this.method = method;

        this.host = host;
        if (host.endsWith("/"))
            this.host = host.substring(0, host.length() - 1);

        this.path = path;
        if (!path.startsWith("/"))
            this.path = path.substring(1);
    }

    public HttpRequest AddHeader(String k, String v) {
        this.header.add(new Pair<>(k, v));
        return this;
    }

    public HttpRequest AddHeader(Pair<String, String> header){
        this.header.add(header);
        return this;
    }


    public HttpRequest AddQuery(String k, String v) {
        this.query.add(new Pair<>(k, v));
        return this;
    }

    public HttpRequest AddBody(String content) {
        if (this.body == null) {
            this.body = content;
        } else if (content != null) {
            this.body += content;
        }
        return this;
    }

    public String Run() throws IOException {
        HttpURLConnection connection = (HttpURLConnection) buildURL().openConnection();

        if(method==Method.POST)
            connection.setRequestMethod("POST");

        // Set Header
        for(Pair<String, String> h : header)
            connection.setRequestProperty(h.getFirst(), h.getSecond());

        // Set Body
        if(body != null){
            connection.setDoOutput(true);
            writeAll(connection.getOutputStream(), body);
        }


        connection.connect();

        String resp = readAll(connection.getInputStream());
        Log.i(InfoLogTag,"Response: " + resp);

        connection.disconnect();

        return resp;
    }

    public <T> T RunWithUnmarshaller(Unmarshaler<T> unmarshaler) throws IOException, UnmarshalException {
        return unmarshaler.Unmarshal(Run());
    }

    private URL buildURL() throws MalformedURLException {
        StringBuilder urlBuilder = new StringBuilder();
        switch (this.protocol) {
            case HTTP:
                urlBuilder.append("http://");break;
            case HTTPS:
                urlBuilder.append("https://");break;
        }
        urlBuilder.append(this.host).append(this.path);
        for (int i = 0; i < query.size(); i++) {
            if (i == 0)
                urlBuilder.append("?");
            else
                urlBuilder.append("&");
            urlBuilder.append(query.get(i).getFirst()).append("=").append(query.get(i).getSecond());
        }
        String url = urlBuilder.toString();
        Log.i(InfoLogTag,"URL - " + url);
        return new URL(url);
    }

    // Common Headers
    public static final Pair<String,String> ContentType_JSON = new Pair<>("Content-Type", "application/json");


    // utils method
    private static String readAll(InputStream inputStream) throws IOException {
        final int bufferSize = 1024;
        final char[] buffer = new char[bufferSize];
        final StringBuilder out = new StringBuilder();
        Reader in = new InputStreamReader(inputStream, "UTF-8");
        for (; ; ) {
            int rsz = in.read(buffer, 0, buffer.length);
            if (rsz < 0)
                break;
            out.append(buffer, 0, rsz);
        }
        in.close();
        return out.toString();
    }

    private static void writeAll(OutputStream outputStream, String content) throws IOException {
        Writer out = new OutputStreamWriter(outputStream, "UTF-8");
        out.write(content);
        out.flush();
        out.close();
    }

    // Unmarshaller
    public interface Unmarshaler<T>  {
        T Unmarshal(String response) throws UnmarshalException;
    }

    public static class UnmarshalException extends Exception {
        public UnmarshalException(Class type, String str) {
            super("Can't Unmarshal To Type: " + type.getCanonicalName() + " From String: \"" + str + "\"");
        }
    }

}
