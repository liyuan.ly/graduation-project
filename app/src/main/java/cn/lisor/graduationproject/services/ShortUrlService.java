package cn.lisor.graduationproject.services;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public abstract class ShortUrlService {
    public static String ShortUrl2TrueUrl(String shortUrl) throws IOException {
        // Create HTTP Connection Object
        HttpURLConnection connection = (HttpURLConnection) new URL(shortUrl).openConnection();
        // Not Allowed Redirect
        connection.setInstanceFollowRedirects(false);
        // Send HTTP GET
        connection.connect();
        // Read Headers of Response
        List<String> locations = connection.getHeaderFields().get("Location");
        // Disconnect
        connection.disconnect();
        if (locations == null || locations.size() == 0) {
            // There is no "Location" field in Headers
            throw new IOException("No Location");
        }
        // Return Value Of Location
        return locations.get(0);
    }
}
