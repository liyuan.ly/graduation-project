package cn.lisor.graduationproject.services;

import com.google.gson.Gson;

import java.io.IOException;

import cn.lisor.graduationproject.utils.HttpRequest;

public abstract class SecurityInfoService {

    public enum Provider {
        Google, Baidu;

        public static Provider New(String s) {
            if ("Google".equals(s)) {
                return Google;
            } else {
                return Baidu;
            }
        }
    }

    public static String Lookup(Provider provider, String url) throws IOException, HttpRequest.UnmarshalException {
        switch (provider) {
            case Google:
                return lookupGoogle(url);
            case Baidu:
            default:
                return lookupBaidu(url);
        }
    }

    private static String lookupBaidu(String url) throws IOException, HttpRequest.UnmarshalException {
        return new HttpRequest(
                HttpRequest.Protocol.HTTP,
                HttpRequest.Method.GET,
                "api.anquan.baidu.com", "/bsb/lookup")
                .AddHeader("apikey", "APIKEY_13f0bff4fd09f644c8715cd366e046e8")
                .AddQuery("url", url)
                .RunWithUnmarshaller(new BaiduType.ResponseBody.Unmarshaler())
                .toString();
    }

    private static String lookupGoogle(String url) throws IOException, HttpRequest.UnmarshalException {
        return new HttpRequest(
                HttpRequest.Protocol.HTTPS,
                HttpRequest.Method.POST,
                "safebrowsing.googleapis.com", "/v4/threatMatches:find")
                .AddQuery("key", "AIzaSyCc_QzpG3Ye70_BjY19JRAX9spM4Q5Dq9k")
                .AddHeader(HttpRequest.ContentType_JSON)
                .AddBody(new GoogleType.RequestBody(url).ToJSON())
                .RunWithUnmarshaller(new GoogleType.ResponseBody.Unmarshaler())
                .toString();
    }

}

/* Google API types */
class GoogleType {
    static class RequestBody {
        ClientInfo client = new ClientInfo();
        ThreatInfo threatInfo;

        RequestBody(String url) {
            this.threatInfo = new ThreatInfo(url);
        }

        String ToJSON() {
            return new Gson().toJson(this);
        }
    }


    static class ThreatInfo {
        ThreatType[] threatTypes = new ThreatType[]{
                ThreatType.THREAT_TYPE_UNSPECIFIED,
                ThreatType.MALWARE,
                ThreatType.SOCIAL_ENGINEERING,
                ThreatType.UNWANTED_SOFTWARE,
                ThreatType.POTENTIALLY_HARMFUL_APPLICATION,
        };
        PlatformType[] platformTypes = new PlatformType[]{
                PlatformType.ANY_PLATFORM,
        };
        ThreatEntryType[] threatEntryTypes = new ThreatEntryType[]{
                ThreatEntryType.URL,
        };

        ThreatEntry[] threatEntries;

        ThreatInfo(String url) {
            this.threatEntries = new ThreatEntry[]{
                    new ThreatEntry(url)
            };
        }
    }

    enum ThreatType {
        //	Unknown
        THREAT_TYPE_UNSPECIFIED,

        //	Malware threat type
        MALWARE,

        //	Social engineering threat type
        SOCIAL_ENGINEERING,

        //	Unwanted software threat type
        UNWANTED_SOFTWARE,

        //	Potentially harmful application threat type
        POTENTIALLY_HARMFUL_APPLICATION
    }

    enum PlatformType {
        // Unknown platform
        PLATFORM_TYPE_UNSPECIFIED,

        // Threat posed to Windows
        WINDOWS,

        // Threat posed to Linux
        LINUX,

        // Threat posed to Android
        ANDROID,

        // Threat posed to OS X.
        OSX,

        // Threat posed to iOS.
        IOS,

        // Threat posed to at least one of the defined platforms.
        ANY_PLATFORM,

        // Threat posed to all defined platforms.
        ALL_PLATFORMS,

        // Threat posed to Chrome.
        CHROME,
    }

    enum ThreatEntryType {
        // Unspecified
        THREAT_ENTRY_TYPE_UNSPECIFIED,

        // 	A URL
        URL,

        // An executable program
        EXECUTABLE,
    }

    static class ThreatEntry {
        String hash;
        String url;
        String digest;

        ThreatEntry(String url) {
            this.url = url;
        }
    }

    static class ClientInfo {
        String clientId;
        String clientVersion;

        ClientInfo() {
            this.clientId = "ly.graduation.project";
            this.clientVersion = "1.0.0";
        }
    }

    static class ResponseBody {
        ThreatMatch[] matches;

        static class Unmarshaler implements HttpRequest.Unmarshaler<ResponseBody> {
            @Override
            public ResponseBody Unmarshal(String response) throws HttpRequest.UnmarshalException {
                return new Gson().fromJson(response, ResponseBody.class);
            }
        }


        public String toString() {
            if (matches == null || matches.length == 0)
                return "未检测到威胁";
            else {
                StringBuilder stringBuilder = new StringBuilder("检测到威胁:");
                for (ThreatMatch match : matches) {
                    stringBuilder.append("\n|")
                            .append(match.threatType)
                            .append(" - ")
                            .append(match.platformType);
                }
                return stringBuilder.toString();
            }
        }
    }

    static class ThreatMatch {
        ThreatType threatType;
        PlatformType platformType;
        ThreatEntryType threatEntryType;
        ThreatEntry threat;
        ThreatEntryMetadata threatEntryMetadata;
        String cacheDuration;
    }

    static class ThreatEntryMetadata {
        MetadataEntry[] entries;
    }

    static class MetadataEntry {
        String key;
        String value;
    }

}

/* Baidu API types */

class BaiduType {
    static class ResponseBody {
        Result[] result;

        static class Unmarshaler implements HttpRequest.Unmarshaler<ResponseBody> {
            @Override
            public ResponseBody Unmarshal(String response) throws HttpRequest.UnmarshalException {
                ResponseBody responseBody = new Gson().fromJson(response, ResponseBody.class);
                if (responseBody.result == null)
                    throw new HttpRequest.UnmarshalException(ResponseBody.class, response);
                return responseBody;
            }
        }

        @Override
        public String toString() {
            String threatType = "";
            String range = "";
            switch (result[0].main) {
                case 0:
                case 1:
                case 2:
                    threatType = "未检测到风险";
                    break;
                case 3:
                    threatType = "欺诈";
                    break;
                case 4:
                    threatType = "风险";
                    break;
                case 5:
                    threatType = "违法";
                    break;
            }
            switch (result[0].range) {
                case 1:
                    range = "站点";
                    break;
                case 2:
                    range = "链接";
                    break;
                case 3:
                    range = "域名";
                    break;
            }
            return range + " - " + threatType;
        }
    }

    static class Result {
        String url;
        // 查询结果作用域 1：站点级别；2：链接级别；3：域名级别
        int range;
        // 查询结果安全主类型（0、1、2：未发现威胁；3：欺诈；4：风险；5：违法）
        int main;
        // 查询结果安全子类型（例如，0x4000000d：虚假购物；0x100000：木马下载）
        int sub;
        // 查询结果安全孙类型（例如，0x832：虚假银行卡）
        int grand;
    }
}