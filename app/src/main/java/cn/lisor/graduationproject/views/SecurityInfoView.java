package cn.lisor.graduationproject.views;

import android.content.Context;

import java.io.IOException;

import cn.lisor.graduationproject.services.SecurityInfoService;
import cn.lisor.graduationproject.utils.HttpRequest;
import cn.lisor.graduationproject.utils.LoadingView;
import cn.lisor.graduationproject.utils.Utils4A;

public class SecurityInfoView extends LoadingView {
    private SecurityInfoService.Provider provider;
    private String url;

    public SecurityInfoView(Context context, SecurityInfoService.Provider provider, String url) {
        this(context, null, provider, url);
    }

    public SecurityInfoView(Context context, String button_name, SecurityInfoService.Provider provider, String url) {
        super(context, button_name);
        this.provider = provider;
        this.url = url;
    }

    @Override
    protected String Do() {
        try {
            return SecurityInfoService.Lookup(provider, url).toString();
        } catch (IOException | HttpRequest.UnmarshalException e) {
            Utils4A.ExceptionLog("SecurityInfoService.Lookup", e);
            return "获取安全信息失败";
        }
    }
}
