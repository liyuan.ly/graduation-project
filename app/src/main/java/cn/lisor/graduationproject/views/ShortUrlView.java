package cn.lisor.graduationproject.views;

import android.content.Context;
import android.util.Log;

import java.io.IOException;

import cn.lisor.graduationproject.services.ShortUrlService;
import cn.lisor.graduationproject.utils.LoadingView;

public class ShortUrlView extends LoadingView {

    private String url;

    public ShortUrlView(Context context, String url) {
        this(context, null, url);
    }

    public ShortUrlView(Context context, String button_name, String url) {
        super(context, button_name);
        this.url = url;
        if(!url.startsWith("http"))
            this.url = "http://" + url;
    }

    @Override
    protected String Do() {
        try {
            return ShortUrlService.ShortUrl2TrueUrl(url);
        } catch (IOException e) {
            Log.w("Exception","ShortUrl2TrueUrl",e);
            return "获取失败";
        }
    }
}
