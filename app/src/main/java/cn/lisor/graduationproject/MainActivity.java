package cn.lisor.graduationproject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.qrcode.QRCodeReader;

import java.io.IOException;

import cn.lisor.graduationproject.services.SecurityInfoService;
import cn.lisor.graduationproject.utils.Utils;
import cn.lisor.graduationproject.utils.Utils4A;
import cn.lisor.graduationproject.views.SecurityInfoView;
import cn.lisor.graduationproject.views.ShortUrlView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.Toolbar);
        setSupportActionBar(toolbar);
    }

    public void Btn_ScanQRCode_OnClick(View v) {
        new IntentIntegrator(this)
                // 设置底部的提示文字，
                .setPrompt("请对准要扫描的二维码")
                // 前置或者后置摄像头
                .setCameraId(0)
                // 关闭扫描成功声音提示
                .setBeepEnabled(false)
                // 指定扫码的Activity
                .setCaptureActivity(ScanActivity.class)
                // 设置要扫描的条码类型，ONE_D_CODE_TYPES：一维码，QR_CODE_TYPES-二维码
                .initiateScan(IntentIntegrator.QR_CODE_TYPES);

    }

    private void onActivityResult_ScanQRCode(int requestCode, int resultCode, Intent data) {
        showQRCodeResult(IntentIntegrator.parseActivityResult(requestCode, resultCode, data).getContents());
    }

    private void showQRCodeResult(String content) {
        if (content != null) {
            // Create LinearLayout
            LinearLayout ll = new LinearLayout(this);
            ll.setOrientation(LinearLayout.VERTICAL);
            ll.setGravity(Gravity.CENTER_HORIZONTAL);
            int _30dp = Utils4A.dp(this, 30);
            ll.setPadding(_30dp, 0, _30dp, 0);


            // Create Dialog
            AlertDialog dialog = new AlertDialog.Builder(this)
                    .setTitle("QRCode Result")
                    .setView(ll)
                    .setNegativeButton("关闭", null)
                    .create();
            dialog.setCanceledOnTouchOutside(false);


            // Create And Add Result Text View
            TextView resultView = new TextView(this);
            resultView.setText(content);
            resultView.setTextIsSelectable(true);
            ll.addView(resultView);


            // Create And Add Security Info View (if need)
            if (Utils.isURL(content)) {
                ll.addView(new SecurityInfoView(this, "获取安全信息", getInfoProvider(), content).GetView());
            }

            dialog.show();
        }
    }

    private static final int ActivityRequestCode_OpenImage = 123;

    public void Btn_OpenQRCode_OnClick(View v) {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, ActivityRequestCode_OpenImage);
    }

    private void onActivityResult_OpenImage(int requestCode, int resultCode, Intent data) {
        try {
            Bitmap bitmap = Utils.GetSmallerBitmap(MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData()));
            int w = bitmap.getWidth();
            int h = bitmap.getHeight();
            int[] p = new int[w * h];
            bitmap.getPixels(p, 0, w, 0, 0, w, h);
            showQRCodeResult(
                    new QRCodeReader().decode(
                            new BinaryBitmap(new HybridBinarizer(new RGBLuminanceSource(w, h, p)))
                    ).getText());
        } catch (NotFoundException | FormatException | ChecksumException e) {
            Toast.makeText(this, "未扫描到有效的QR Code!", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Toast.makeText(this, "打开图片失败!", Toast.LENGTH_LONG).show();
        }
    }


    public void Btn_ShortUrl_OnClick(View v) {
        String url = Utils4A.GetTextByViewId(this, R.id.Txt_ShortUrl_Ipt);
        if (url.equals(""))
            Toast.makeText(this, "输入不可为空", Toast.LENGTH_LONG).show();
        else
            new ShortUrlView(this, url).ShowAsDialog("获取短网址的实际网址");

    }


    public void Btn_SecurityUrl_OnClick(View v) {
        String url = Utils4A.GetTextByViewId(this, R.id.Txt_SecurityUrl_Ipt);
        if (url.equals(""))
            Toast.makeText(this, "输入不可为空", Toast.LENGTH_LONG).show();
        else
            new SecurityInfoView(this, getInfoProvider(), url)
                    .ShowAsDialog("获取安全信息");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Canceled
        if (resultCode != RESULT_OK) return;

        if (requestCode == ActivityRequestCode_OpenImage)
            onActivityResult_OpenImage(requestCode, resultCode, data);
        else if (requestCode == IntentIntegrator.REQUEST_CODE)
            onActivityResult_ScanQRCode(requestCode, resultCode, data);
        else
            super.onActivityResult(requestCode, resultCode, data);
    }


    private SecurityInfoService.Provider getInfoProvider() {
        return SecurityInfoService.Provider.New((String) ((Spinner) findViewById(R.id.SecurityInfoProvider)).getSelectedItem());
    }

}

