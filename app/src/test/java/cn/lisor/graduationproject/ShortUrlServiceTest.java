package cn.lisor.graduationproject;

import org.junit.Test;

import java.io.IOException;

import cn.lisor.graduationproject.services.ShortUrlService;

import static org.junit.Assert.*;

public class ShortUrlServiceTest {

    @Test
    public void shortUrl2TrueUrl() throws IOException {
        assertEquals(
                "https://www.baidu.com",
                ShortUrlService.ShortUrl2TrueUrl("https://dwz.cn/xkqPRyJH")
        );
    }
}