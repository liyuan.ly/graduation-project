package cn.lisor.graduationproject;

import org.junit.Test;

import cn.lisor.graduationproject.utils.Utils;

import static org.junit.Assert.*;

public class UtilsTest {

    @Test
    public void isURL() {
        assertTrue(Utils.isURL("http://x"));
        assertTrue(Utils.isURL("https://x"));
        assertFalse(Utils.isURL("www.a"));
    }
}